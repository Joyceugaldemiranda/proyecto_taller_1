from google.colab import drive
drive.mount('/content/drive')

import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw, ImageFont

def show_image(image_path):
  """ This function displays an image.
     Arguments:
         image_path {str} -- Path of the image.
   """
  img = cv2.imread(image_path)
  img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  plt.imshow(img_cvt)

def show_image_with_square(image_path, analysis):
    """ This function displays an image with a frame on each face.
        Arguments:
            image_path {str} -- Path of the image.
            analysis {str} -- Image information obtained from Microsoft Azure
    """
    img = cv2.imread(image_path)
    im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    imTemp = im2Display.copy()

    faces = []
    for face in analysis:
        fr = face['faceRectangle']
        faces.append(fr)
        top = fr['top']
        left = fr['left']
        width = fr['width']
        height = fr['height']
        print(top, left, width, height)
        pt1 = (left, top )
        pt2 = (left+width, top+height)
        
        color = (23,200,54)
        thickness = 10
        cv2.rectangle(imTemp, pt1, pt2, color, thickness)
    plt.imshow(imTemp)
    plt.show()

#!pip install dlib
#!pip install cognitive_face 
import sys
import requests
import json

import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

#Get Microsoft Azure Credentials
#https://azure.microsoft.com/en-us/free/students/

SUBSCRIPTION_KEY = 'aa72faec54314f11bac4a277c8b0fdff'                   #It will be enabled only on the day of the workshop
BASE_URL = 'https://jenny-progra.cognitiveservices.azure.com/'  #It will be enabled only on the day of the workshop
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)

def emotions(picture):
  """ This function obtains information about the people in a photo.
     Arguments:
         picture {str} -- Path of the image.

     Returns:
        analysis {} -- The information of the people in the photo.
   """
  image_path = picture
  # Read the image into a byte array
  image_data = open(image_path, "rb").read()
  headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
  'Content-Type': 'application/octet-stream'}
  params = {
      'returnFaceId': 'true',
      'returnFaceLandmarks': 'false',
      'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
  }
  response = requests.post(
                            BASE_URL + "detect/", headers=headers, params=params, data=image_data)
  analysis = response.json()
  return analysis

def create_group(group_id, group_name):
    """Create a new group of people.
        Arguments:
            group_id {int} -- is the group id
            group_name {str} -- is the name of the group

        You only have to create it the first time
        """
    CF.person_group.create(group_id, group_name)
    print("Grupo creado")

def delete_group(group_id):
    """Delete an existing group of people.
    Arguments:
        group_id {int} -- is the group id

    """
    res = CF.person_group.delete(group_id)
    print("Grupo eliminado",res)

def create_person(name, profession, picture, group_id):
    """Create a person in a group
    Only one person should come in the photo
    Arguments:
        name {str} -- is the name of the person
        profession {str} -- is the person's profession
        picture {str} -- is the path of the person's picture
        group_id {str} -- is the group to which you want to add the person
    """
    response = CF.person.create(group_id, name, profession)
    #print(response)
    #En response viene el person_id de la persona que se ha creado
    # Obtener person_id de response
    person_id = response['personId']
    #print(person_id)
    #Sumarle una foto a la persona que se ha creado
    CF.person.add_face(picture, group_id, person_id)
    #print CF.person.lists(PERSON_GROUP_ID)
    
    #Re-entrenar el modelo porque se le agrego una foto a una persona
    CF.person_group.train(group_id)
    #Obtener el status del grupo
    response = CF.person_group.get_status(group_id)
    status = response['status']
    print(status)

def add_picture_to_person(picture, group_id, person_id):
    """Add a photo to a person and train the model again.
    Only one person should come in the photo.
    Arguments:
        picture {str} -- is the path of the person's picture
        group_id {str} -- is the group to which you want to add the person
        person_id {str} -- is the id of the person to add the photo to
    """
    #Sumarle una foto a la persona que se ha creado
    CF.person.add_face(picture, group_id, person_id)
    #print CF.person.lists(PERSON_GROUP_ID)
    
    #Re-entrenar el modelo porque se le agrego una foto a una persona
    CF.person_group.train(group_id)
    #Obtener el status del grupo
    response = CF.person_group.get_status(group_id)
    status = response['status']
    print(status)

def print_people(group_id):
    """#Print the list of people belonging to a group
     Arguments:
         group_id {str} -- is the id of the group whose people you want to print
     """
    #Imprimir la lista de personas del grupo
    print(CF.person.lists(group_id))

def recognize_person(image_path, group_id):
  """Recognize people by photo
   Arguments:
       picture {str} -- is the path of the photo of the person you want to recognize
       group_id {str} -- is the id of the group in which you want to search for the person
   """
  print(image_path, " ", group_id)
  #Detentando los rostros en las fotos
  response = CF.face.detect(image_path)
  for face in response:
    print(face['faceId'])

  # Obteniendo los ids de los rostros en las fotos
  face_ids = [d['faceId'] for d in response]
  print("id de los rostros", face_ids)
  if face_ids != []:
    # Identificar personas en con esos rostros
    identified_faces = CF.face.identify(face_ids, group_id)
    for person in identified_faces:
      faceId = person['faceId']
      candidates_list = person['candidates']
      #.....print("candidates_list: ", candidates_list)
      for candidate in candidates_list:
          personId = candidate['personId']
          #...print(personId, faceId)
          person_data = CF.person.get(group_id, personId)
          person_name = person_data['name']
          print(person_name)
          for face in response:
            faceId2 = face['faceId']
            if faceId2 == faceId:
              faceRectangle = face['faceRectangle']
              width = faceRectangle['width']
              top = faceRectangle['top']
              height = faceRectangle['height']
              left = faceRectangle['left']

              img = cv2.imread(image_path)
              im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
              imTemp = im2Display.copy()

              pt1 = (left, top )
              pt2 = (left+width, top+height)
    
              color = (23,200,54)
              thickness = 10
              cv2.rectangle(imTemp, pt1, pt2, color, thickness)

              #plt.imshow(imTemp)
              # plt.show()

def recognize_person2(image_path, group_id):
  """Recognize people by photo
   Arguments:
       picture {str} -- is the path of the photo of the person you want to recognize
       group_id {str} -- is the id of the group in which you want to search for the person
   """
  personId=0
  person_name=""
  #Detentando los rostros en las fotos
  response = CF.face.detect(image_path)

  # Obteniendo los ids de los rostros en las fotos
  face_ids = [d['faceId'] for d in response]
  if face_ids != []:
    # Identificar personas en con esos rostros
    identified_faces = CF.face.identify(face_ids, group_id)
    for person in identified_faces:
      faceId = person['faceId']
      candidates_list = person['candidates']
      #.....print("candidates_list: ", candidates_list)
      for candidate in candidates_list:
          personId = candidate['personId']
          #...print(personId, faceId)
          person_data = CF.person.get(group_id, personId)
          person_name = person_data['name']
          for face in response:
            faceId2 = face['faceId']
            if faceId2 == faceId:
              faceRectangle = face['faceRectangle']
              width = faceRectangle['width']
              top = faceRectangle['top']
              height = faceRectangle['height']
              left = faceRectangle['left']

              img = cv2.imread(image_path)
              im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
              imTemp = im2Display.copy()

              pt1 = (left, top )
              pt2 = (left+width, top+height)
    
              color = (23,200,54)
              thickness = 10
              cv2.rectangle(imTemp, pt1, pt2, color, thickness)

              #plt.imshow(imTemp)
              # plt.show()
  return [personId,person_name]

#!pip install dlib
#!pip install cognitive_face 
import sys
import requests
import json

import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

SUBSCRIPTION_KEY = '1d1fd6a70c444cac8d6e0a421460719f'
BASE_URL = 'https://mis-clases.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)

import cv2
'''Opens a video with which frames are generated and the frame is saved
      Arguments:
          cv2.VideoCapture{str}—Video path.
          cv2.imwrite{str}—Path to the folder to save frames, with name.
          image_path{str}—Path to store frame.
      returns:
          analysis {} – Frame of the candidates with their data.
'''
# Opens the Video file
video= cv2.VideoCapture('/content/drive/MyDrive/videos/video 1.mp4')
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
i=0
j = 30
while(video.isOpened()):  #abre el video
    ret, frame = video.read() #obtiene cada fotograma 

    if ret == False:  #si no obtiene ningún fotograma se termina el ciclo
        break

    if i == j:     #obtenemos el fotograma número 94
      cv2.imwrite('/content/drive/MyDrive/imágenes video 1/'+ 'nombre'+str(i)+'.jpg',frame) #se guarda el fotograma con el nombre más el número del contador i
      j +=30
      image_path = '/content/drive/MyDrive/imágenes video 1/'+ 'nombre'+str(i)+'.jpg'
      analysis = emotions(image_path)
      print(analysis)
      show_image_with_square(image_path, analysis)
    i+=1

video.release()

import cv2
'''Opens a video with which frames are generated and the frame is saved
      Arguments:
          cv2.VideoCapture{str}—Video path.
          cv2.imwrite{str}—Path to the folder to save frames, with name.
          image_path{str}—Path to store frame.
      returns:
          analysis {} – Frame of the candidates with their data.
'''
# Opens the Video file
video= cv2.VideoCapture('/content/drive/MyDrive/videos/Video 2.mp4')
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
i=0
j =30
while(video.isOpened()):  #abre el video
    ret, frame = video. read() #obtiene cada fotograma 

    if ret == False:  #si no obtiene ningún fotograma se termina el ciclo
        break

    if i == j:     #obtenemos el fotograma número 94
      cv2.imwrite('/content/drive/MyDrive/imagenes video 2/'+ 'nombre'+str(i)+'.jpg',frame) #se guarda el fotograma con el nombre más el número del contador i
      j +=30
      image_path = '/content/drive/MyDrive/imagenes video 2/'+ 'nombre'+str(i)+'.jpg'
      analysis = emotions(image_path)
      print(analysis)
      show_image_with_square(image_path, analysis)
    i+=1

video.release()

import cv2
'''Opens a video with which frames are generated and the frame is saved
      Arguments:
          cv2.VideoCapture{str}—Video path.
          cv2.imwrite{str}—Path to the folder to save frames, with name.
          image_path{str}—Path to store frame.
      returns:
          analysis {} – Frame of the candidates with their data.
'''
# Opens the Video file
video= cv2.VideoCapture('/content/drive/MyDrive/videos/Video #3.mp4')
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
i=0
j =30
while(video.isOpened()):  #abre el video
    ret, frame = video.read() #obtiene cada fotograma 

    if ret == False:  #si no obtiene ningún fotograma se termina el ciclo
        break

    if i == j:     #obtenemos el fotograma número 94
      cv2.imwrite('/content/drive/MyDrive/imagenes video 3/'+ 'nombre'+str(i)+'.jpg',frame) #se guarda el fotograma con el nombre más el número del contador i
      j +=30
      image_path = '/content/drive/MyDrive/imagenes video 3/'+ 'nombre'+str(i)+'.jpg'
      analysis = emotions(image_path)
      print(analysis)
      show_image_with_square(image_path, analysis)
    i+=1

video.release()

"""Create groups in Microsoft Azure
Arguments:
create_group {str}—group number and description
        returns:
           group created
"""
create_group(18, "Candidatos  videos")  

"""Create groups in Microsoft Azure
Arguments:
create_group {str}—group number and description
        returns:
           group created
"""
create_group(19, "Candidatos  videos") 

"""Create groups in Microsoft Azure
Arguments:
create_group {str}—group number and description
        returns:
           group created
"""
create_group(21, "Candidatos  videos") 

"""Delete groups in Microsoft Azure
Arguments:
create_group {str}—group number
        returns:
           deleted group
"""
delete_group(18)

"""Delete groups in Microsoft Azure
Arguments:
create_group {str}—group number
        returns:
           deleted group
"""
delete_group(19)

"""Delete groups in Microsoft Azure
Arguments:
create_group {str}—group number
        returns:
           deleted group
"""
delete_group(21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Figueres 1.jpeg"
show_image(picture_path)
create_person("José María Figueres", "Candidato Presidencial",  picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imágenes video 1/nombre1620.jpg"
show_image(picture_path)
create_person("José María Figueres", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Figueres 3.jpeg"
show_image(picture_path)
create_person("José María Figueres", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Figueres 4.jpeg"
show_image(picture_path)
create_person("José María Figueres", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Figueres 5.jpeg"
show_image(picture_path)
create_person("José María Figueres", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Rodrigo 2.jpeg"
show_image(picture_path)
create_person("Rodrigo Chavez", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Rodrigo 3.jpeg"
show_image(picture_path)
create_person("Rodrigo Chavez", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imágenes video 1/nombre11010.jpg"
show_image(picture_path)
create_person("Rodrigo Chavez", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Copia de Rodrigo_Chaves_Robles_2020.png"
show_image(picture_path)
create_person("Rodrigo Chavez", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 1/Rodrigo.jpeg"
show_image(picture_path)
create_person("Rodrigo Chavez", "Candidato Presidencial", picture_path, 18)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre10620.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre11640.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre11580.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre11700.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre10680.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre16890.jpg"
show_image(picture_path)
create_person("Walter Muñoz Céspedes", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre17250.jpg"
show_image(picture_path)
create_person("Walter Muñoz Céspedes", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre17640.jpg"
show_image(picture_path)
create_person("Walter Muñoz Céspedes", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre17970.jpg"
show_image(picture_path)
create_person("Walter Muñoz Céspedes", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre2250.jpg"
show_image(picture_path)
create_person("Walter Muñoz Céspedes", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre7020.jpg"
show_image(picture_path)
create_person("Rodolfo Piza", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre7710.jpg"
show_image(picture_path)
create_person("Rodolfo Piza", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre8220.jpg"
show_image(picture_path)
create_person("Rodolfo Piza", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre8250.jpg"
show_image(picture_path)
create_person("Rodolfo Piza", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre8460.jpg"
show_image(picture_path)
create_person("Rodolfo Piza", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre60.jpg"
show_image(picture_path)
create_person("Christian Aaron Rivera", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre9300.jpg"
show_image(picture_path)
create_person("Christian Aaron Rivera", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre9810.jpg"
show_image(picture_path)
create_person("Christian Aaron Rivera", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre990.jpg"
show_image(picture_path)
create_person("Christian Aaron Rivera", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre9510.jpg"
show_image(picture_path)
create_person("Christian Aaron Rivera", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre14070.jpg"
show_image(picture_path)
create_person("José María Villalta", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre19200.jpg"
show_image(picture_path)
create_person("José María Villalta", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre19770.jpg"
show_image(picture_path)
create_person("José María Villalta", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre19230.jpg"
show_image(picture_path)
create_person("José María Villalta", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes video 2/nombre19950.jpg"
show_image(picture_path)
create_person("José María Villalta", "Candidato Presidencial", picture_path, 19)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Martin-Chinchilla-Castro.png"
show_image(picture_path)
create_person("Martín Chinchilla", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Martin_Chinchilla-1-1.jpg"
show_image(picture_path)
create_person("Martín Chinchilla", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/chinchilla.jpg"
show_image(picture_path)
create_person("Martín Chinchilla", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/chinchilla.png"
show_image(picture_path)
create_person("Martín Chinchilla", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/martín-chinchilla-candidato-LpXlMCFjsyP-BP8bYlHgXK3.300x300.jpg"
show_image(picture_path)
create_person("Martín Chinchilla", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/welmwer.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/welmer.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Welmer.jpeg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Welmer-ramos-gonzalez_41148243775_o_(cropped).png"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/conozca-a-welmer-ramos-candidato-a-presidente-por-el-pac_826900355_1140x520.jpg"
show_image(picture_path)
create_person("Welmer Ramos", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Fabricio-740x416.png"
show_image(picture_path)
create_person("Fabricio Alvarado", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Fabricio Alvarado Muñoz_Nueva República.png"
show_image(picture_path)
create_person("Fabricio Alvarado", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/alvarado.jpeg"
show_image(picture_path)
create_person("Fabricio Alvarado", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/alvarado.jpg"
show_image(picture_path)
create_person("Fabricio Alvarado", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/P-20-FabricioCancela.jpg"
show_image(picture_path)
create_person("Fabricio Alvarado", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Federico Malavassi Calvo_Unión Liberal.jpg"
show_image(picture_path)
create_person("Federico Malavassi", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Federico-Malavassi-e1642766354112.jpg"
show_image(picture_path)
create_person("Federico Malavassi", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Federico_Malavassi.jpg"
show_image(picture_path)
create_person("Federico Malavassi", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/malavassi.jpg"
show_image(picture_path)
create_person("Federico Malavassi", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/circles_federicomalavassi.png"
show_image(picture_path)
create_person("Federico Malavassi", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Carmen-Quesada-Liberacion.jpg"
show_image(picture_path)
create_person("Carmen Quesada", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Carmen-Quesada..png"
show_image(picture_path)
create_person("Carmen Quesada", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/carmen (1).jpg"
show_image(picture_path)
create_person("Carmen Quesada", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/carmen (2).jpg"
show_image(picture_path)
create_person("Carmen Quesada", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/carmen.jpg"
show_image(picture_path)
create_person("Carmen Quesada", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Greivin Moya Carpio_Fuerza Nacional.jpg"
show_image(picture_path)
create_person("Greivin Moya", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/Greivin-Moya-Carpio1.png"
show_image(picture_path)
create_person("Greivin Moya", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/greivin (1).png"
show_image(picture_path)
create_person("Greivin Moya", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/greivin-moya-candidato-presidencial-fuerza.jpg"
show_image(picture_path)
create_person("Greivin Moya", "Candidato Presidencial", picture_path,21)

"""Add candidates to each group, their name and their formation.
Arguments:
picture_path{str}—Image to recognize.
show_image(picture_path)--frame
create_person {str, int}--Name, formation, group number.
        returns:
           analysis {} – Frame, candidate added
"""
picture_path = "/content/drive/MyDrive/imagenes de reconocimiento/video 3/greivin.png"
show_image(picture_path)
create_person("Greivin Moya", "Candidato Presidencial", picture_path,21)

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis1{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imágenes video 1/nombre3030.jpg"
recognize_person(image_path, 18)
show_image(image_path)
analysis1 = emotions(image_path)
show_image_with_square(image_path, analysis1)
print(analysis1)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_figueres():
  for face in analysis1:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_figueres())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis2{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imágenes video 1/nombre11010.jpg"
recognize_person(image_path, 18)
show_image(image_path)
analysis2 = emotions(image_path)
show_image_with_square (image_path, analysis2)
print(analysis2)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_chavez():
  for face in analysis2:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_chavez())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis3{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre30.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis3 = emotions(image_path)
show_image_with_square(image_path, analysis3)
print(analysis3)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_aaron():
  for face in analysis3:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_aaron())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis4{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre16890.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis4 = emotions(image_path)
show_image_with_square(image_path, analysis4)
print(analysis4)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_walter():
  for face in analysis4:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_walter())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis5{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre7710.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis5 = emotions(image_path)
show_image_with_square(image_path, analysis5)
print(analysis5)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_piza():
  for face in analysis5:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_piza())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis6{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre11580.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis6 = emotions(image_path)
show_image_with_square(image_path, analysis6)
print(analysis6)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_welmer():
  for face in analysis6:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_welmer())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis7{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre14070.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis7 = emotions(image_path)
show_image_with_square(image_path, analysis7)
print(analysis7)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_villalta():
  for face in analysis7:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_villalta())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis8{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre1500.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis8 = emotions(image_path)
show_image_with_square(image_path, analysis8)
print(analysis8)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_greivin():
  for face in analysis8:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_greivin())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis9{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre7950.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis9 = emotions(image_path)
show_image_with_square(image_path, analysis9)
print(analysis9)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_malavassi():
  for face in analysis9:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_malavassi())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis0{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre11190.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis0 = emotions(image_path)
show_image_with_square(image_path, analysis0)
print(analysis0)


"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_carmen():
  for face in analysis0:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_carmen())

"""Separate queries for each group of frames
Arguments:
image_path{str}: image path.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
show_image_with_square{}--frame, analysis
          returns:
             analysis11{}--frame with a box on the face and its respective analysis.
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre2220.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis11 = emotions(image_path)
show_image_with_square(image_path, analysis11)
print(analysis11)

"""candidate's age
        returns:
           --the function enters the analysis and returns the age of the candidate.
"""
def edad_martin():
  for face in analysis11:
    faceId= face['faceId']
    faceAttributes = face['faceAttributes']
    age = faceAttributes['age']
    return faceId, int(age)
print(edad_martin())

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis1–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis1:
  rostros=len(analysis1)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis2–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis2:
  rostros=len(analysis2)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis3–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis3:
  rostros=len(analysis3)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis4–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis4:
  rostros=len(analysis4)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis5–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis5:
  rostros=len(analysis5)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis6–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis6:
  rostros=len(analysis6)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis7–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis7:
  rostros=len(analysis7)
  faceAttributes = face['faceAttributes']
  age = faceAttributes['age']
print('La imagen contiene:', rostros, 'rostros y su edad es:', int(age))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis8–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis8:
  rostros=len(analysis8)
  faceAttributes = face['faceAttributes']
  if rostros!=1:
    age = []
    cont=0
    while len(age)<rostros:
      age.append(analysis9[cont]['faceAttributes']['age'])
      cont +=1

print('La imagen contiene:', rostros, 'rostros y sus edades son:')
for edades in age:
    print(int(edades))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis9–Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis9:
  rostros=len(analysis9)
  faceAttributes = face['faceAttributes']
  if rostros!=1:
    age = []
    cont=0
    while len(age)<rostros:
      age.append(analysis9[cont]['faceAttributes']['age'])
      cont +=1

print('La imagen contiene:', rostros, 'rostros y sus edades son:')
for edades in age:
    print(int(edades))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis0–-Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis0:
  rostros=len(analysis0)
  faceAttributes = face['faceAttributes']
  if rostros!=1:
    age = []
    cont=0
    while len(age)<rostros:
      age.append(analysis0[cont]['faceAttributes']['age'])
      cont +=1

print('La imagen contiene:', rostros, 'rostros y sus edades son:')
for edades in age:
    print(int(edades))

"""Entering the analysis of each image, return the number of faces and their age.
Arguments:
analysis11–-Image information obtained from Microsoft Azure
           returns:
number of faces and their age
"""
for face in analysis11:
  rostros=len(analysis11)
  faceAttributes = face['faceAttributes']
  if rostros!=1:
    age = []
    cont=0
    while len(age)<rostros:
      age.append(analysis11[cont]['faceAttributes']['age'])
      cont +=1

print('La imagen contiene:', rostros, 'rostros y sus edades son:')
for edades in age:
    print(int(edades))

"""List with face ID and ages of the candidates.
             returns:
list
"""
lista=[edad_figueres(), edad_chavez(), edad_aaron(), edad_walter(), edad_piza(), edad_welmer(), edad_villalta(), edad_greivin(), edad_malavassi(), edad_carmen(), edad_martin()]
print(lista)

"""the function receives a list, it is traversed to compare its values and determine the smallest.
Arguments:
list-- list of data to compare
            returns:
minor of the list
"""
def menor(lista):
  if lista != []:
    primero=lista[0][1]
    cuenta = 1
    while  cuenta <  len(lista):
      if lista[cuenta][1] < primero:
        primero=lista[cuenta][1]
      cuenta +=1
    return primero
  else:
    return []
  
print(menor(lista))

"""function receives a list, loops through it to compare its values and determine the largest.
Arguments:
list-- list of data to compare
            returns:
The biggest on the list
"""
def mayor(lista):
  primero = lista[0][1]
  registro = 1
  while registro < len(lista):
    if lista[registro][1] > primero :
      primero = lista[registro][1]
    registro +=1
  return primero
print(mayor(lista))

"""Returns the youngest and oldest among the candidates.
"""
print('La menor edad entre los candidatos es de: ', menor(lista), 'años, y la mayor edad entre los candidatos es de: ', mayor(lista), 'años.')

"""Se crea una lista con face Id, FaceAttributes y Age, obtenidos desde un analisis.
Arguments:
analisis1-- Image information obtained from Microsoft Azure
            returns:
lista con Face Id, Attributes, Age
"""
lista_candidato1 = []
for face in analysis1:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato1.append(face_id)
    lista_candidato1.append(age)
    lista_candidato1.append(gender)
print(lista_candidato1)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis2-- Image information obtained from Microsoft Azure
              returns:
list with Face Id, Attributes, Age
"""
lista_candidato2 = []
for face in analysis2:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato2.append(face_id)
    lista_candidato2.append(age)
    lista_candidato2.append(gender)
print(lista_candidato2)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis3-- Image information obtained from Microsoft Azure
              returns:
list with Face Id, Attributes, Age
"""
lista_candidato3 = []
for face in analysis3:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato3.append(face_id)
    lista_candidato3.append(age)
    lista_candidato3.append(gender)
print(lista_candidato3)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis4-- Image information obtained from Microsoft Azure
              returns:
list with Face Id, Attributes, Age
"""
lista_candidato4 = []
for face in analysis4:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato4.append(face_id)
    lista_candidato4.append(age)
    lista_candidato4.append(gender)
print(lista_candidato4)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis5-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato5 = []
for face in analysis5:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato5.append(face_id)
    lista_candidato5.append(age)
    lista_candidato5.append(gender)
print(lista_candidato5)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis6-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato6 = []
for face in analysis6:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato6.append(face_id)
    lista_candidato6.append(age)
    lista_candidato6.append(gender)
print(lista_candidato6)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis7-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato7 = []
for face in analysis7:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato7.append(face_id)
    lista_candidato7.append(age)
    lista_candidato7.append(gender)
print(lista_candidato7)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis8-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato8 = []
for face in analysis8:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato8.append(face_id)
    lista_candidato8.append(age)
    lista_candidato8.append(gender)
print(lista_candidato8)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis9-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato9 = []
for face in analysis9:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato9.append(face_id)
    lista_candidato9.append(age)
    lista_candidato9.append(gender)
print(lista_candidato9)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis0-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato0 = []
for face in analysis0:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato0.append(face_id)
    lista_candidato0.append(age)
    lista_candidato0.append(gender)
print(lista_candidato0)

"""A list is created with face Id, FaceAttributes and Age, obtained from an analysis.
Arguments:
analysis0-- Image information obtained from Microsoft Azure
        returns:
list with Face Id, Attributes, Age
"""
lista_candidato11 = []
for face in analysis11:
    face_id = face['faceId']
    faceAttributes = face['faceAttributes']
    gender = faceAttributes['gender']
    age = faceAttributes['age']
    lista_candidato11.append(face_id)
    lista_candidato11.append(age)
    lista_candidato11.append(gender)
print(lista_candidato11)

"""Insert the lists with faceId, age and gender.
Arguments:
list: stores the sublists with the face ID, attributes and age of each candidate.
           returns:
list with sublists
"""
lista = [lista_candidato1, lista_candidato2, lista_candidato3, lista_candidato4, lista_candidato5, lista_candidato6, lista_candidato7, lista_candidato8, lista_candidato9, lista_candidato0, lista_candidato11]
#for x in lista:
  #print(x,"\n")
for x in lista:
  if len(x)>3:
    del x[3:]
print(lista)

"""function receives a list, loops through it to compare its values and determine the largest.
Arguments:
list-- list of data to compare
            returns:
The biggest on the list
"""
def mayor(lista):
  primero = lista[0][1]
  registro = 1
  reg_tem= 0
  while registro < len(lista):
    if lista[registro][1] > primero :
      primero = lista[registro][1]
      reg_tem= registro
    registro +=1
  return reg_tem
print(mayor(lista))

def ordenamiento_insercion(lista):
    """Insertion-based sorting
      Arguments:
         list {list} -- Unordered List
      Returns:
         new_list {list} -- Ordered List
     """
    nueva_lista = []
    while len(lista)>0:
        mayor_lista= mayor(lista)
        nueva_lista.insert(0,lista[mayor_lista])
        lista.remove(lista[mayor_lista])
    return nueva_lista

print(ordenamiento_insercion([lista_candidato1, lista_candidato2, lista_candidato3, lista_candidato4, lista_candidato5, lista_candidato6, lista_candidato7, lista_candidato8, lista_candidato9, lista_candidato0, lista_candidato11]))

""" Create list with attributes of analysis0.
      Arguments:
          analysis0--Image information obtained from Microsoft Azure
     return:
       list with FaceId, age, gender.
    """
listaf = []
faceAttributes = analysis0[0]['faceAttributes']
gender = faceAttributes['gender']
age = faceAttributes['age']
listaf.append(age)
listaf.append(gender)
print(listaf)

lista_m = []
""" Create list with attributes in sublists of different analyses.
      Arguments:
          analysis--Image information obtained from Microsoft Azure
     return:
       list with FaceId, age, gender.
    """
for x in analysis1: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)

for x in analysis2: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)

for x in analysis3: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)

for x in analysis4: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)

for x in analysis5: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)

for x in analysis6: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)

for x in analysis7: #review analysis
  faceAttributes = x['faceAttributes']
  gender = faceAttributes["gender"]
  age = faceAttributes["age"]
  lista_temp=[]
  lista_temp.append(gender)
  lista_temp.append(age)
  lista_m.append(lista_temp)


faceAttributes = analysis8[0]['faceAttributes'] #review analysis
gender = faceAttributes["gender"]
age = faceAttributes["age"]
lista_temp=[]
lista_temp.append(gender)
lista_temp.append(age)
lista_m.append(lista_temp)
 
faceAttributes = analysis9[0]['faceAttributes'] #review analysis
gender = faceAttributes["gender"]
age = faceAttributes["age"]
lista_temp=[]
lista_temp.append(gender)
lista_temp.append(age)
lista_m.append(lista_temp)
 
faceAttributes = analysis11[0]['faceAttributes'] #review analysis
gender = faceAttributes["gender"]
age = faceAttributes["age"]
lista_temp=[]
lista_temp.append(gender)
lista_temp.append(age)
lista_m.append(lista_temp)

print(lista_m)

"""function receives a list, loops through it to compare its values and determine the largest.
Arguments:
list_m-- list of data to compare
            returns:
The biggest on the list
"""
def mayor(lista_m):
  primero = lista_m[0][1]
  registro = 1
  reg_tem= 0
  while registro < len(lista_m):
    if lista_m[registro][1] > primero :
      primero = lista_m[registro][1]
      reg_tem= registro
    registro +=1
  return reg_tem
print(mayor(lista_m))

def ordenamiento_insercion(lista_m):
    """Insertion-based sorting
     Arguments:
         list_m {list} -- Unordered List
     Returns:
         new_list {list} -- Ordered List
     """
    nueva_lista = []
    while len(lista_m)>0:
        mass= mayor(lista_m)
        nueva_lista.insert(0,lista_m[mass])
        
        lista_m.remove(lista_m[mass])
    return nueva_lista

"""Ordered list, female gender first, then male gender, sorted ascending
      Arguments:
          listf {} -- female list
          listm{}-- male list
      returns:
           total_list -- Ordered List
"""
lista_total=[listaf, ordenamiento_insercion(lista_m)]
print(lista_total)

"""Check if the candidate has accessories
       Arguments:
           analysis1 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis1[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

  """Check if the candidate has accessories
       Arguments:
           analysis2 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis2[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis3 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis3[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)
"""Check if the candidate has accessories
       Arguments:
           analysis4 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis4[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis5 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis5[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis6 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis6[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis7 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis7[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis8 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis8[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis9 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis9[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis0 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis0[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

"""Check if the candidate has accessories
       Arguments:
           analysis11 {} -- Image information obtained from Microsoft Azure
       returns:
            accessories -- list of accessories
            None-- no accessories
       """
faceAttributes = analysis11[0]['faceAttributes']
accessories= faceAttributes['accessories']
if accessories == []:
  print(None, 'accessories')
else:
  print(accessories)

""" This function displays an image with a different color frame depending on the gender
    Arguments:
        image_path {str} -- Image path.
        analysis {str} -- Image information obtained from Microsoft Azure
"""
def show_image_with_squares(image_path, analysis):
 
  img = cv2.imread(image_path)
  im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  imTemp = im2Display.copy()
  
  faces = []
  for face in analysis:
    fr = face['faceRectangle']
    faceAttributes= face['faceAttributes']
    gender= faceAttributes['gender']
    faces.append(fr)
    top = fr['top']
    left = fr['left']
    width = fr['width']
    height = fr['height']
    print(top, left, width, height)
    pt1 = (left, top )
    pt2 = (left+width, top+height)
    
    if gender == 'female': #if it's female
      color = (12,43,200)
      thickness = 10
      cv2.rectangle(imTemp, pt1, pt2, color, thickness)
    else: #if it's male
      color = (200,56,94)
      thickness = 10
      cv2.rectangle(imTemp, pt1, pt2, color, thickness)
  plt.imshow(imTemp)
  plt.show()

  """Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis1{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imágenes video 1/nombre3030.jpg"
recognize_person(image_path, 18)
show_image(image_path)
analysis1 = emotions(image_path)
show_image_with_squares(image_path, analysis1)
print(analysis1)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis2{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imágenes video 1/nombre11010.jpg"
recognize_person(image_path, 18)
show_image(image_path)
analysis2 = emotions(image_path)
show_image_with_squares (image_path, analysis2)
print(analysis2)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis3{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre30.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis3 = emotions(image_path)
show_image_with_squares(image_path, analysis3)
print(analysis3)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis4{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre16890.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis4 = emotions(image_path)
show_image_with_squares(image_path, analysis4)
print(analysis4)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis5{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre7710.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis5 = emotions(image_path)
show_image_with_squares(image_path, analysis5)
print(analysis5)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis6{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre11580.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis6 = emotions(image_path)
show_image_with_squares(image_path, analysis6)
print(analysis6)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis7{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre14070.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis7 = emotions(image_path)
show_image_with_squares(image_path, analysis7)
print(analysis7)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis8{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre1500.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis8 = emotions(image_path)
show_image_with_squares(image_path, analysis8)
print(analysis8)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis9{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre7950.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis9 = emotions(image_path)
show_image_with_squares(image_path, analysis9)
print(analysis9)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis0{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre11190.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis0 = emotions(image_path)
show_image_with_squares(image_path, analysis0)
print(analysis0)

"""Recognize the candidate's image and assign it a color box, depending on its gender
Arguments:
image_path{str}: path of the image.
recognize_person{str}—Recognize the frame in a group.
show_image{}—image
emotions(image_path) -- This function obtains information about the people in a photo.
show_image_with_square{}-- square, analysis
           returns:
              analysis11{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre2220.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis0 = emotions(image_path)
show_image_with_squares(image_path, analysis11)
print(analysis11)

def show_image_with_squar(image_path, analysis):
  """ This function displays an image with a frame on each face that contains a percentage greater than zero in its
   beard, mustache, sideburns.
     Arguments:
         image_path {str} -- Path of the image.
         analysis {str} -- Image information obtained from Microsoft Azure
   """
  img = cv2.imread(image_path)
  im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  imTemp = im2Display.copy()

  #plt.imshow(imTemp)
  #plt.show()
  
  faces = []
  fr = analysis[0]['faceRectangle']
  faceAttributes= analysis[0]['faceAttributes']
  facialHair= faceAttributes['facialHair']
  moustache= facialHair['moustache']
  
  beard= facialHair ['beard']
  sideburns= facialHair ['sideburns']
  faces.append(fr)
  top = fr['top']
  left = fr['left']
  width = fr['width']
  height = fr['height']
  #print(top, left, width, height)
  pt1 = (left, top )
  pt2 = (left+width, top+height)
  if moustache > 0.0 and beard > 0.0 and sideburns > 0.0:
    color = (200,56,94)
    thickness = 10
    cv2.rectangle(imTemp, pt1, pt2, color, thickness)
    print('Tiene barba, mostacho, patillas') 
  elif moustache == 0.0 and beard == 0.0 and sideburns == 0.0:
    print('No tiene barba, mostacho, patillas') 
  plt.imshow(imTemp)
  plt.show()

  """Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis1{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imágenes video 1/nombre3030.jpg"
recognize_person(image_path, 18)
show_image(image_path)
analysis1 = emotions(image_path)
show_image_with_squar(image_path, analysis1)
print(analysis1)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis2{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imágenes video 1/nombre11010.jpg"
recognize_person(image_path, 18)
show_image(image_path)
analysis2 = emotions(image_path)
show_image_with_squar(image_path, analysis2)
print(analysis2)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis3{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre30.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis3 = emotions(image_path)
show_image_with_squar(image_path, analysis3)
print(analysis3)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis4{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre16890.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis4 = emotions(image_path)
show_image_with_squar(image_path, analysis4)
print(analysis4)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis5{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre7710.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis5 = emotions(image_path)
show_image_with_squar(image_path, analysis5)
print(analysis5)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis6{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre11580.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis6 = emotions(image_path)
show_image_with_squar(image_path, analysis6)
print(analysis6)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis7{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 2/nombre14070.jpg"
recognize_person(image_path, 19)
show_image(image_path)
analysis7 = emotions(image_path)
show_image_with_squar(image_path, analysis7)
print(analysis7)
"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
show_image{}—image
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis8{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre1500.jpg"
recognize_person(image_path, 21)
show_image(image_path)
analysis8 = emotions(image_path)
show_image_with_squar(image_path, analysis8)
print(analysis8)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis9{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre7950.jpg"
recognize_person(image_path, 21)
analysis9 = emotions(image_path)
show_image_with_squar(image_path, analysis9)
print(analysis9)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis0{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre11190.jpg"
recognize_person(image_path, 21)
analysis0 = emotions(image_path)
show_image_with_squar(image_path, analysis0)
print(analysis0)

"""Recognize the candidate's image and assign a color box to it, if it contains a beard, mustache and sideburns
Arguments:
image_path{str}: image path.
recognize_person{str}: Recognize the image in a group.
emotions (image_path): This function gets information about the people in a photo.
show_image_with_square{}-- square, analysis
             returns:
                analysis0{}--image of the candidate with its respective box, analysis
"""
image_path = "/content/drive/MyDrive/imagenes video 3/nombre2220.jpg"
recognize_person(image_path, 21)
analysis11 = emotions(image_path)
show_image_with_squar(image_path, analysis11)
print(analysis11)

"""Recognize the amount of baldness that each candidate presents
Arguments:
analysis–-Image information obtained from Microsoft Azure
              returns:
                 baldness_list--list with the amount of baldness of each candidate
"""
print('Lista de la cantidad de calvicia de cada candidato:')
faceAttributes = analysis1[0]['faceAttributes']
hair= faceAttributes['hair']
bald1 = hair["bald"]

faceAttributes = analysis2[0]['faceAttributes']
hair= faceAttributes['hair']
bald2 = hair["bald"]

faceAttributes = analysis3[0]['faceAttributes']
hair= faceAttributes['hair']
bald3 = hair["bald"]

faceAttributes = analysis4[0]['faceAttributes']
hair= faceAttributes['hair']
bald4 = hair["bald"]

faceAttributes = analysis5[0]['faceAttributes']
hair= faceAttributes['hair']
bald5 = hair["bald"]

faceAttributes = analysis6[0]['faceAttributes']
hair= faceAttributes['hair']
bald6 = hair["bald"]

faceAttributes = analysis7[0]['faceAttributes']
hair= faceAttributes['hair']
bald7 = hair["bald"]

faceAttributes = analysis8[0]['faceAttributes']
hair= faceAttributes['hair']
bald8 = hair["bald"]

faceAttributes = analysis9[0]['faceAttributes']
hair= faceAttributes['hair']
bald9 = hair["bald"]

faceAttributes = analysis0[0]['faceAttributes']
hair= faceAttributes['hair']
bald0 = hair["bald"]

faceAttributes = analysis11[0]['faceAttributes']
hair= faceAttributes['hair']
bald11 = hair["bald"]

lista_calvicia=['Candidato 1°=> ', bald1, 'Candidato 2°=> ', bald2, 'Candidato 3°=> ', bald3, 'Candidato 4°=> ', bald4, 'Candidato 5°=> ', bald5, 'Candidato 6°=> ', bald6, 'Candidato 7°=> ', bald7, 'Candidato 8°=> ', bald8, 'Candidato 9°=> ', bald9, 'Candidato 0°=> ', bald0, 'candidato 11°=> ', bald11]
print(lista_calvicia)


"""Recognize the amount of joy that each candidate presents
Arguments:
analysis–-Image information obtained from Microsoft Azure
               returns:
                  happiness_list--list with amount of happiness
"""
lista_happiness=[]
faceAttributes = analysis1[0]['faceAttributes']
faceId= analysis1[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist1=[happy,faceId]
lista_happiness.append(sublist1)

faceAttributes = analysis2[0]['faceAttributes']
faceId= analysis2[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist2=[happy,faceId]
lista_happiness.append(sublist2)

faceAttributes = analysis3[0]['faceAttributes']
faceId= analysis3[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist3=[happy,faceId]
lista_happiness.append(sublist3)

faceAttributes = analysis4[0]['faceAttributes']
faceId= analysis4[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist4=[happy,faceId]
lista_happiness.append(sublist4)

faceAttributes = analysis5[0]['faceAttributes']
faceId= analysis5[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist5=[happy,faceId]
lista_happiness.append(sublist5)

faceAttributes = analysis6[0]['faceAttributes']
faceId= analysis6[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist6=[happy,faceId]
lista_happiness.append(sublist6)

faceAttributes = analysis7[0]['faceAttributes']
faceId= analysis7[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist7=[happy,faceId]
lista_happiness.append(sublist7)

faceAttributes = analysis8[0]['faceAttributes']
faceId= analysis8[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist8=[happy,faceId]
lista_happiness.append(sublist8)

faceAttributes = analysis9[0]['faceAttributes']
faceId= analysis9[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist9=[happy,faceId]
lista_happiness.append(sublist9)

faceAttributes = analysis0[0]['faceAttributes']
faceId= analysis0[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist=[happy,faceId]
lista_happiness.append(sublist)

faceAttributes = analysis11[0]['faceAttributes']
faceId= analysis11[0]['faceId']
emotion= faceAttributes['emotion']
happy = emotion["happiness"]
sublist=[happy,faceId]
lista_happiness.append(sublist)

print(lista_happiness)

"""The function receives a list, iterates through it to compare its joy values and determine the largest.
Arguments:
list_happiness-- list of data to compare
            returns:
The biggest of the joy list
"""
def mayor(lista_happiness):
  primero = lista_happiness[0]
  registro = 1
  while registro < len(lista_happiness):
    if lista_happiness[registro] > primero :
      primero = lista_happiness[registro]
    registro +=1
  return primero
print('La mayor cantidad de alegría con su respectivo Id de la imagen es:')
print(mayor(lista_happiness))

import cv2
'''Opens a video with which frames are generated and the frame is saved
      Arguments:
          cv2.VideoCapture{str}—Video path.
          cv2.imwrite{str}—Path to the folder to save frames, with name.
          image_path{str}—Path to store frame.
      returns:
          analysis {} – Frame of the candidates with their data.
'''
# Opens the Video file
video= cv2.VideoCapture('/content/drive/MyDrive/videos/video 1.mp4')
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
i=0
j = 300
while(video.isOpened()):  #abre el video
    ret, frame = video.read() #obtiene cada fotograma 

    if ret == False:  #si no obtiene ningún fotograma se termina el ciclo
        break

    if i == j:     #obtenemos el fotograma número 94
      cv2.imwrite('/content/drive/MyDrive/Imagenes 9.1/'+ 'nombre'+str(i)+'.jpg',frame) #se guarda el fotograma con el nombre más el número del contador i
      j +=30
      image_path = '/content/drive/MyDrive/Imagenes 9.1/'+ 'nombre'+str(i)+'.jpg'
      analysis = emotions(image_path)
      #print(analysis)
      #show_image_with_square(image_path, analysis)
    i+=1

video.release()

import cv2
'''Opens a video with which frames are generated and the frame is saved
      Arguments:
          cv2.VideoCapture{str}—Video path.
          cv2.imwrite{str}—Path to the folder to save frames, with name.
          image_path{str}—Path to store frame.
      returns:
          analysis {} – Frame of the candidates with their data.
'''
# Opens the Video file
video= cv2.VideoCapture('/content/drive/MyDrive/videos/Video 2.mp4')
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
i=0
j =300
while(video.isOpened()):  #abre el video
    ret, frame = video. read() #obtiene cada fotograma 

    if ret == False:  #si no obtiene ningún fotograma se termina el ciclo
        break

    if i == j:     #obtenemos el fotograma número 94
      cv2.imwrite('/content/drive/MyDrive/Imagenes 9.2/'+ 'nombre'+str(i)+'.jpg',frame) #se guarda el fotograma con el nombre más el número del contador i
      j +=30
      image_path = '/content/drive/MyDrive/Imagenes 9.2/'+ 'nombre'+str(i)+'.jpg'
      analysis = emotions(image_path)
      #print(analysis)
      #show_image_with_square(image_path, analysis)
    i+=1

video.release()

import cv2
'''Opens a video with which frames are generated and the frame is saved
      Arguments:
          cv2.VideoCapture{str}—Video path.
          cv2.imwrite{str}—Path to the folder to save frames, with name.
          image_path{str}—Path to store frame.
      returns:
          analysis {} – Frame of the candidates with their data.
'''
# Opens the Video file
video= cv2.VideoCapture('/content/drive/MyDrive/videos/Video #3.mp4')
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
i=0
j =30
while(video.isOpened()):  #abre el video
    ret, frame = video.read() #obtiene cada fotograma 

    if ret == False:  #si no obtiene ningún fotograma se termina el ciclo
        break

    if i == j:     #obtenemos el fotograma número 94
      cv2.imwrite('/content/drive/MyDrive/Imagenes 9.3/'+ 'nombre'+str(i)+'.jpg',frame) #se guarda el fotograma con el nombre más el número del contador i
      j +=30
      image_path = '/content/drive/MyDrive/Imagenes 9.3/'+ 'nombre'+str(i)+'.jpg'
      analysis = emotions(image_path)
      #print(analysis)
      #show_image_with_square(image_path, analysis)
    i+=1

video.release()

"""This function returns a list with the emotions of each candidate.
Arguments:
analysis--Image information obtained from Microsoft Azure
             returns:
list with emotions
"""
def insertar_emociones(analysis):
  lista=[]
  
  lista.append("anger")
  lista.append(analysis[0]['faceAttributes']['emotion']['anger'])
  lista.append("contempt")
  lista.append(analysis[0]['faceAttributes']['emotion']['contempt'])
  lista.append("disgust")
  lista.append(analysis[0]['faceAttributes']['emotion']['disgust'])
  lista.append("fear")
  lista.append(analysis[0]['faceAttributes']['emotion']['fear'])
  lista.append("happiness")
  lista.append(analysis[0]['faceAttributes']['emotion']['happiness'])
  lista.append("neutral")
  lista.append(analysis[0]['faceAttributes']['emotion']['neutral'])
  lista.append("sadness")
  lista.append(analysis[0]['faceAttributes']['emotion']['sadness'])
  lista.append("surprise")
  lista.append(analysis[0]['faceAttributes']['emotion']['surprise'])

  return lista

  """The function receives the address of the image and an empty list where the data is saved, saving the emotions and people for each video.
Arguments:
image_path--image path
list--empty list
              returns:
list of emotions and people of each video
"""
def datos_videos(image_path,lista):
  analysis = emotions(image_path)
  #print("analisys: ",analysis)
  personId=0
  name=""
  if analysis !=[]:
    if recognize_person2(image_path,18)[0]!=0:
      personId=recognize_person2(image_path,18)[0]
      name=recognize_person2(image_path,18)[1]

    elif recognize_person2(image_path,19)[0]!=0:
      personId=recognize_person2(image_path,19)[0]
      name=recognize_person2(image_path,19)[1]

    elif recognize_person2(image_path,21)[0]!=0:
      personId=recognize_person2(image_path,21)[0]
      name=recognize_person2(image_path,21)[1]
      

    if len(lista)==0:
      lista.append([])
      lista[0].append([])
      lista[0][0].append(personId)
      lista[0][0].append(name)
      lista[0][0].append(analysis[0]['faceAttributes']['gender'])
      lista[0][0].append(analysis[0]['faceAttributes']['age'])
      lista[0].append(insertar_emociones(analysis))

    else:
      valida=False
      for x in lista:
        if x != None:
          #print("X: ",x)
          if name==x[0][1]:
            x.append(insertar_emociones(analysis))
            valida=True
      if valida == False:
        print("")
        lista.append([])
        lista[-1].append([])
        lista[-1][0].append(personId)
        lista[-1][0].append(name)
        lista[-1][0].append(analysis[0]['faceAttributes']['gender'])
        lista[-1][0].append(analysis[0]['faceAttributes']['age'])
        lista[-1].append(insertar_emociones(analysis))

"""Go through the generated images of the first video to pass them to data_videos.
Arguments:
It does not have, it is not a function
                returns:
print the list with the return data_video
"""
# Opens the Video file
#video= cv2.VideoCapture('/content/drive/MyDrive/Colab Notebooks/Tec-Clases/II-Sem-2021/Taller de Programación/Semana 8/Abel-2-Entrenar-31-ago-2020.MOV')
j = 300
lista_candidatos_emociones1=[]
#17310
while(j<=18120):  #open the video 
  image_path = '/content/drive/MyDrive/Imagenes 9.1/'+ 'nombre'+str(j)+'.jpg'
  #print(image_path)
  lista_temp=[]
  lista_temp=datos_videos(image_path,lista)
  if lista_temp != None:
    lista_candidatos_emociones1.append(lista_temp)
  j +=30

print(lista_candidatos_emociones1)

"""Go through the generated images of the second video to pass them to data_videos.
Arguments:
It does not have, it is not a function
                returns:
print the list with the return data_video
"""
j = 300
lista_candidatos_emociones2=[]
#19950
while(j<=19950):  #open the video
  image_path = '/content/drive/MyDrive/Imagenes 9.2/'+ 'nombre'+str(j)+'.jpg'
  #print(image_path)
  lista_temp=[]
  lista_temp=datos_videos(image_path,lista_candidatos_emociones2)
  if lista_temp != None:
    lista_candidatos_emociones2.append(lista_temp)
  j +=30

print(lista_candidatos_emociones2)

"""Go through the generated images of the third video to pass them to data_videos.
Arguments:
It does not have, it is not a function
                returns:
print the list with the return data_video
"""
j = 30
lista_candidatos_emociones3=[]
#find all images
while(j<=300):  #abre el video
  image_path = '/content/drive/MyDrive/imagenes video 3/'+ 'nombre'+str(j)+'.jpg'
  #print(image_path)
  lista_temp=[]
  lista_temp=datos_videos(image_path,lista_candidatos_emociones3)
  if lista_temp != None:
    lista_candidatos_emociones3.append(lista_temp)
  j +=30

print(lista_candidatos_emociones3)

"""splits the list of candidates with their emotions, by the emotion of happiness, extracting the smallest value in a sublist,
the medium and the largest in another sublist
Arguments:
list--list to be partitioned
position--to change candidate
last--to know the depth of the list
                returns:
returns a list with minors, middle, majors
"""
def particiona_lista(lista,posicion,pasada):
  menores=list()
  mayores=list()
  #print("pivote: ", lista[posicion][-1])
  if pasada == False:
    pivote=lista[posicion][-1]
    for x in lista[posicion][1:-1]:
    #print(x)
      if x[9]<pivote[9]:
        menores.append(x)
      else:
        mayores.append(x)
  else:
    pivote=lista[-1]
    for x in lista[1:-1]:
    #print(x)
      if x[9]<pivote[9]:
        menores.append(x)
      else:
        mayores.append(x)
  return([menores,pivote,mayores])

"""This function consists of sorting the list with the QuickSort method, the function that partitions the list is called, thus achieving sorting.
Arguments:
list--list to sort
partition_list(list,position,last)--function to partition the list
                 returns:
returns ordered list
"""
def ordenamiento_quickSort(lista):
  posicion=0
  pasada=False
  for x in lista:
    if len(x)>0:
      lista_particionada=particiona_lista(lista,posicion,pasada)
      #for y in lista_particionada:
      #  print(y,"\n")
      cont=0
      pasada=True
      while cont<len(lista_particionada):
        e=lista_particionada[cont]
        #print("e: ",e)
        #print(type(e))
        valida=False
        for y in e:
          if type(y) == list:
            valida=True
        if valida==True:
          if len(e)==0:
              del lista_particionada[cont]
          elif len(e)==1:
              lista_particionada[cont]=e[0]
              cont = cont + 1
          elif e :
              lista_particionada=lista_particionada[:cont]+particiona_lista(e,posicion,pasada)+lista_particionada[cont+1:]
        else:
            cont=cont+1
    del x[1:]
    
    for y in lista_particionada:
      if len(y) == 0:
        lista_particionada.remove(y)
    #print("particionada: ", lista_particionada)
    x.append(lista_particionada)
    posicion=+1
    
  return lista

"""Call the sort function by the QuickSort method, to print the list of the first video"
Arguments:
list--list to sort
                  returns:
print the sorted list of the first video
"""
lista_ordenada=ordenamiento_quickSort(lista_candidatos_emociones1)
for x in lista_ordenada:
  print(x)

  """Call the sort function by the QuickSort method, to print the list of the second video"
Arguments:
list--list to sort
                  returns:
print the sorted list of the second video
"""
lista_ordenada2=ordenamiento_quickSort(lista_candidatos_emociones2)
for x in lista_ordenada2:
  print(x)

  """Call the sort function by the quicksort method, to print the list of the third video"
Arguments:
list--list to sort
                  returns:
print the sorted list of the third video
"""
lista_ordenada3=ordenamiento_quickSort(lista_candidatos_emociones3)
for x in lista_ordenada3:
  print(x)

"""This function receives a list with sublists, with faceId, name, gender, age and emotions of candidates with their respective position
Arguments:
list-- list of data to compare
post-- position
         returns:
list with amount of anger
list with personal data
"""
def enojo(lista, pos):
  lista_final_persona=[]
  lista_anger=[]
  for x in lista[pos]:
    #print(y)
    if len(x)==4:
      lista_final_persona.append(x)
    elif len(x)> 4:
      lista_anger.append(x[1])
  return[lista_anger, lista_final_persona]

  """This function brings out the 5 largest amounts of anger.
Arguments:
list-- list with amount of anger
             returns:
List with the 5 greatest amounts of sadness
"""
def mayor(lista_anger):
  lista_5_mayores=[]
  cont= 0
  while cont < 5:
    primero = lista_anger[0]
    registro = 1
    while registro < len(lista_anger):
      if lista_anger[registro] > primero :
        primero = lista_anger[registro]
      registro +=1
    lista_5_mayores.append(primero)
    lista_anger.remove(primero)
    cont +=1
  return lista_5_mayores

"""Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of anger
"""
for index, x in enumerate(lista):
  lista_enojo= enojo(lista,index)[0]
  lista_datos_personas=enojo(lista,index)[1]
  print("Mayores enojos de ",lista_datos_personas[0][1],":\n",mayor(lista_enojo))

"""This function receives a list with sublists, with faceId, name, gender, age and emotions of candidates with their respective position
Arguments:
list-- list of data to compare
post-- position
         returns:
list with amount of sadness
list with personal data
"""
def triste(lista, pos):
  lista_final_persona=[]
  lista_sadness=[]
  for x in lista[pos]:
    #print(y)
    if len(x)==4:
      lista_final_persona.append(x)
    elif len(x)> 4:
      lista_sadness.append(x[13])
  return[lista_sadness, lista_final_persona]

  """This function brings out the 5 largest amounts of sadness.
Arguments:
list-- list with amount of sadness
             returns:
List with the 5 greatest amounts of sadness
"""
def mayor(lista_sadness):
  lista_5_mayores=[]
  cont= 0
  while cont < 5:
    primero = lista_sadness[0]
    registro = 1
    while registro < len(lista_sadness):
      if lista_sadness[registro] > primero :
        primero = lista_sadness[registro]
      registro +=1
    lista_5_mayores.append(primero)
    lista_sadness.remove(primero)
    cont +=1
  return lista_5_mayores

  """Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of sadness
"""
for index, x in enumerate(lista_candidatos_emociones1):
  lista_triste= triste(lista_candidatos_emociones1,index)[0]
  lista_datos_personas=triste(lista_candidatos_emociones1,index)[1]
  print("Mayores tristezas de ",lista_datos_personas[0][1],":\n",mayor(lista_triste))

  """Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of sadness
"""
for index, x in enumerate(lista_candidatos_emociones2):
  lista_triste= triste(lista_candidatos_emociones2,index)[0]
  lista_datos_personas=triste(lista_candidatos_emociones2,index)[1]
  print("Mayores tristezas de ",lista_datos_personas[0][1],":\n",mayor(lista_triste))

  """Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of sadness
"""
for index, x in enumerate(lista_candidatos_emociones3):
  lista_triste= triste(lista_candidatos_emociones3,index)[0]
  lista_datos_personas=triste(lista_candidatos_emociones3,index)[1]
  print("Mayores tristezas de ",lista_datos_personas[0][1],":\n",mayor(lista_triste))

  """This function receives a list with sublists, with faceId, name, gender, age and emotions of candidates with their respective position
Arguments:
list-- list of data to compare
post-- position
         returns:
list with amount of happiness
list with personal data
"""
def alegria(lista, pos):
  lista_final_persona=[]
  lista_happiness=[]
  for x in lista[pos]:
    #print(y)
    if len(x)==4:
      lista_final_persona.append(x)
    elif len(x)> 4:
      lista_happiness.append(x[9])
  return[lista_happiness, lista_final_persona]

  """This function brings out the 5 largest amounts of happy.
Arguments:
list-- list with amount of sadness
             returns:
List with the 5 greatest amounts of happy
"""
def mayor(lista_happiness):
  lista_5_mayores=[]
  cont= 0
  while cont < 5:
    primero = lista_happiness[0]
    registro = 1
    while registro < len(lista_happiness):
      if lista_happiness[registro] > primero :
        primero = lista_happiness[registro]
      registro +=1
    lista_5_mayores.append(primero)
    lista_happiness.remove(primero)
    cont +=1
  return lista_5_mayores

  """Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of happy
"""
for index, x in enumerate(lista_candidatos_emociones1):
  lista_alegre= alegria(lista_candidatos_emociones1,index)[0]
  lista_datos_personas=alegria(lista_candidatos_emociones1,index)[1]
  print("Mayores alegrias de ",lista_datos_personas[0][1],":\n",mayor(lista_alegre))

  """Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of happy
"""
for index, x in enumerate(lista_candidatos_emociones2):
  lista_alegre= alegria(lista_candidatos_emociones2,index)[0]
  lista_datos_personas=alegria(lista_candidatos_emociones2,index)[1]
  print("Mayores alegrias de ",lista_datos_personas[0][1],":\n",mayor(lista_alegre))

  """Gives the position of the list
              returns:
Name of the candidate with his 5 major emotions of happy
"""
for index, x in enumerate(lista_candidatos_emociones3):
  lista_alegre= alegria(lista_candidatos_emociones3,index)[0]
  lista_datos_personas=alegria(lista_candidatos_emociones3,index)[1]
  print("Mayores alegrias de ",lista_datos_personas[0][1],":\n",mayor(lista_alegre))

  """Recognize the amount of anger that each candidate presents in certain images.
Arguments:
analysis: image information obtained from Microsoft Azure
                returns:
          anger_list: list with the amount of anger
"""
lista_Anger=[]
faceAttributes = analysis1[0]['faceAttributes']
faceId= analysis1[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist1=[anger,faceId]
lista_Anger.append(sublist1)

faceAttributes = analysis2[0]['faceAttributes']
faceId= analysis2[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist2=[anger,faceId]
lista_Anger.append(sublist2)

faceAttributes = analysis3[0]['faceAttributes']
faceId= analysis3[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist3=[anger,faceId]
lista_Anger.append(sublist3)

faceAttributes = analysis4[0]['faceAttributes']
faceId= analysis4[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist4=[anger,faceId]
lista_Anger.append(sublist4)

faceAttributes = analysis5[0]['faceAttributes']
faceId= analysis5[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist5=[anger,faceId]
lista_Anger.append(sublist5)

faceAttributes = analysis6[0]['faceAttributes']
faceId= analysis6[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist6=[anger,faceId]
lista_Anger.append(sublist6)

faceAttributes = analysis7[0]['faceAttributes']
faceId= analysis7[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist7=[anger,faceId]
lista_Anger.append(sublist7)

faceAttributes = analysis8[0]['faceAttributes']
faceId= analysis8[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist8=[anger,faceId]
lista_Anger.append(sublist8)

faceAttributes = analysis9[0]['faceAttributes']
faceId= analysis9[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist9=[anger,faceId]
lista_Anger.append(sublist9)

faceAttributes = analysis0[0]['faceAttributes']
faceId= analysis0[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist=[anger,faceId]
lista_Anger.append(sublist)

faceAttributes = analysis11[0]['faceAttributes']
faceId= analysis11[0]['faceId']
emotion= faceAttributes['emotion']
anger = emotion["anger"]
sublist=[anger,faceId]
lista_Anger.append(sublist)

print(lista_Anger)

"""The function receives a list, iterates through it to compare its joy values and determine the largest.
Arguments:
list_happiness-- list of data to compare
            returns:
The biggest of the joy list
"""
def cantidad_enojo(lista_Anger):
  lista_cantidad_enojo=[]
  for anger in lista_Anger:
    lista_cantidad_enojo.append(anger[0])
  return lista_cantidad_enojo
#print(cantidad_enojo(lista_Anger))

"""Develop graph with percentages of anger, taken from specific images, produced by Microsoft Azure.
Arguments:
list_Anger-- list with percentage of anger
             returns:
Graph with the amount of anger
"""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

#Definimos una lista con paises como string
candidatos = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
#Definimos una lista con ventas como entero
porcentaje_enojo = cantidad_enojo(lista_Anger)

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Porcentaje de enojo')
#Colocamos una etiqueta en el eje X
ax.set_title('Cantidad de Enojo de los candidatos')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(candidatos, porcentaje_enojo)
plt.savefig('barras_simple.png')
#Finalmente mostramos la grafica con el metodo show()
plt.show()
print('1=> Jose María Figueres')
print('2=> Rodrigo Chavez')
print('3=> Christian Aaron Rivera')
print('4=> Walter Muñoz')
print('5=> Rodolfo Piza')
print('6=> Welmer Ramos')
print('7=> Jose María Villalta')
print('8=> Greiven Moya')
print('9=> Federico Malavassi')
print('10=> Carmen Quesada')
print('11=> Martín Chinchilla')

